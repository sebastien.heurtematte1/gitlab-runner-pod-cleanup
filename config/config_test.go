package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/logging"
)

func TestNewConfig(t *testing.T) {
	defautVerification := func(t *testing.T, c, expected Config) {
		interval, err := c.GetInterval()
		assert.NoError(t, err)
		expectedInterval, err := expected.GetInterval()
		assert.NoError(t, err)

		cacheCleanupInterval, err := c.GetCacheCleanupInterval()
		assert.NoError(t, err)
		expectedCacheCleanupInterval, err := expected.GetCacheCleanupInterval()
		assert.NoError(t, err)

		cacheExpiration, err := c.GetCacheExpiration()
		assert.NoError(t, err)
		expectedCacheExpiration, err := expected.GetCacheExpiration()
		assert.NoError(t, err)

		requestTimeout, err := c.GetRequestTimeout()
		assert.NoError(t, err)
		expectedRequestTimeout, err := expected.GetRequestTimeout()
		assert.NoError(t, err)

		assert.Equal(t, c, expected)
		assert.Equal(t, interval, expectedInterval)
		assert.Equal(t, cacheCleanupInterval, expectedCacheCleanupInterval)
		assert.Equal(t, cacheExpiration, expectedCacheExpiration)
		assert.Equal(t, requestTimeout, expectedRequestTimeout)
		assert.Equal(t, c.Kubernetes.Annotation, expected.Kubernetes.Annotation)
		assert.Equal(t, c.Kubernetes.Namespaces, expected.Kubernetes.Namespaces)
	}
	tests := map[string]struct {
		getConfig      func() Config
		expectedConfig Config
		verify         func(*testing.T, Config, Config)
	}{
		"generate default config": {
			getConfig: func() Config {
				os.Setenv("POD_CLEANUP_KUBERNETES_ANNOTATION", "pod-cleanup/ttl")
				config := New()
				os.Unsetenv("POD_CLEANUP_KUBERNETES_ANNOTATION")
				return config
			},
			expectedConfig: Config{
				LogLevel:             "info",
				LogFormat:            logging.FormatJSON,
				Interval:             "60s",
				CacheCleanupInterval: "30m",
				CacheExpiration:      "1.5h",
				Limit:                15,
				MaxErrAllowed:        5,
				Kubernetes: KubernetesConfig{
					RequestTimeout: "30s",
					RequestLimit:   500,
					Namespaces:     []string{"default"},
					Annotation:     "pod-cleanup/ttl",
				},
			},
			verify: defautVerification,
		},
		"generate custom config": {
			getConfig: func() Config {
				os.Setenv("POD_CLEANUP_LOG_LEVEL", "debug")
				os.Setenv("POD_CLEANUP_INTERVAL", "5s")
				os.Setenv("POD_CLEANUP_KUBERNETES_ANNOTATION", "pod-cleanup/ttl")
				config := New()
				os.Unsetenv("POD_CLEANUP_LOG_LEVEL")
				os.Unsetenv("POD_CLEANUP_INTERVAL")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_ANNOTATION")
				return config
			},
			expectedConfig: Config{
				LogLevel:             "debug",
				LogFormat:            logging.FormatJSON,
				Interval:             "5s",
				CacheCleanupInterval: "30m",
				CacheExpiration:      "1.5h",
				Limit:                15,
				MaxErrAllowed:        5,
				Kubernetes: KubernetesConfig{
					RequestTimeout: "30s",
					RequestLimit:   500,
					Namespaces:     []string{"default"},
					Annotation:     "pod-cleanup/ttl",
				},
			},
			verify: defautVerification,
		},
		"generate invalid interval": {
			getConfig: func() Config {
				os.Setenv("POD_CLEANUP_LOG_LEVEL", "debug")
				os.Setenv("POD_CLEANUP_INTERVAL", "faulty")
				os.Setenv("POD_CLEANUP_KUBERNETES_ANNOTATION", "pod-cleanup/ttl")
				config := New()
				os.Unsetenv("POD_CLEANUP_LOG_LEVEL")
				os.Unsetenv("POD_CLEANUP_INTERVAL")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_ANNOTATION")
				return config
			},
			expectedConfig: Config{
				LogLevel:             "debug",
				LogFormat:            logging.FormatJSON,
				Interval:             "faulty",
				CacheCleanupInterval: "30m",
				CacheExpiration:      "1.5h",
				Limit:                15,
				MaxErrAllowed:        5,
				Kubernetes: KubernetesConfig{
					RequestTimeout: "30s",
					RequestLimit:   500,
					Namespaces:     []string{"default"},
					Annotation:     "pod-cleanup/ttl",
				},
			},
			verify: func(t *testing.T, c, expected Config) {
				_, err := c.GetInterval()
				assert.Error(t, err)

				assert.Equal(t, c.Kubernetes.Annotation, expected.Kubernetes.Annotation)
				assert.Equal(t, c.Kubernetes.Namespaces, expected.Kubernetes.Namespaces)
			},
		},
		"generate invalid cache cleanup interval": {
			getConfig: func() Config {
				os.Setenv("POD_CLEANUP_LOG_LEVEL", "debug")
				os.Setenv("POD_CLEANUP_CACHE_CLEANUP_INTERVAL", "faulty")
				os.Setenv("POD_CLEANUP_KUBERNETES_ANNOTATION", "pod-cleanup/ttl")
				config := New()
				os.Unsetenv("POD_CLEANUP_LOG_LEVEL")
				os.Unsetenv("POD_CLEANUP_CACHE_CLEANUP_INTERVAL")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_ANNOTATION")
				return config
			},
			expectedConfig: Config{
				LogLevel:             "debug",
				LogFormat:            logging.FormatJSON,
				Interval:             "60s",
				CacheCleanupInterval: "faulty",
				CacheExpiration:      "1.5h",
				Limit:                15,
				MaxErrAllowed:        5,
				Kubernetes: KubernetesConfig{
					RequestTimeout: "30s",
					RequestLimit:   500,
					Namespaces:     []string{"default"},
					Annotation:     "pod-cleanup/ttl",
				},
			},
			verify: func(t *testing.T, c, expected Config) {
				_, err := c.GetCacheCleanupInterval()
				assert.Error(t, err)

				assert.Equal(t, c.Kubernetes.Annotation, expected.Kubernetes.Annotation)
				assert.Equal(t, c.Kubernetes.Namespaces, expected.Kubernetes.Namespaces)
			},
		},
		"generate invalid cache expiration": {
			getConfig: func() Config {
				os.Setenv("POD_CLEANUP_LOG_LEVEL", "debug")
				os.Setenv("POD_CLEANUP_CACHE_EXPIRATION", "faulty")
				os.Setenv("POD_CLEANUP_KUBERNETES_ANNOTATION", "pod-cleanup/ttl")
				config := New()
				os.Unsetenv("POD_CLEANUP_LOG_LEVEL")
				os.Unsetenv("POD_CLEANUP_CACHE_EXPIRATION")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_ANNOTATION")
				return config
			},
			expectedConfig: Config{
				LogLevel:             "debug",
				LogFormat:            logging.FormatJSON,
				Interval:             "60s",
				CacheCleanupInterval: "30m",
				CacheExpiration:      "faulty",
				Limit:                15,
				MaxErrAllowed:        5,
				Kubernetes: KubernetesConfig{
					RequestTimeout: "30s",
					RequestLimit:   500,
					Namespaces:     []string{"default"},
					Annotation:     "pod-cleanup/ttl",
				},
			},
			verify: func(t *testing.T, c, expected Config) {
				_, err := c.GetCacheExpiration()
				assert.Error(t, err)

				assert.Equal(t, c.Kubernetes.Annotation, expected.Kubernetes.Annotation)
				assert.Equal(t, c.Kubernetes.Namespaces, expected.Kubernetes.Namespaces)
			},
		},
		"generate invalid request timeout": {
			getConfig: func() Config {
				os.Setenv("POD_CLEANUP_LOG_LEVEL", "debug")
				os.Setenv("POD_CLEANUP_KUBERNETES_REQUEST_TIMEOUT", "faulty")
				os.Setenv("POD_CLEANUP_KUBERNETES_ANNOTATION", "pod-cleanup/ttl")
				config := New()
				os.Unsetenv("POD_CLEANUP_LOG_LEVEL")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_REQUEST_TIMEOUT")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_ANNOTATION")
				return config
			},
			expectedConfig: Config{
				LogLevel:             "debug",
				LogFormat:            logging.FormatJSON,
				Interval:             "60s",
				CacheCleanupInterval: "30m",
				CacheExpiration:      "1.5h",
				Limit:                15,
				MaxErrAllowed:        5,
				Kubernetes: KubernetesConfig{
					RequestTimeout: "faulty",
					RequestLimit:   200,
					Namespaces:     []string{"default"},
					Annotation:     "pod-cleanup/ttl",
				},
			},
			verify: func(t *testing.T, c, expected Config) {
				_, err := c.GetRequestTimeout()
				assert.Error(t, err)

				assert.Equal(t, c.Kubernetes.Annotation, expected.Kubernetes.Annotation)
				assert.Equal(t, c.Kubernetes.Namespaces, expected.Kubernetes.Namespaces)
			},
		},
		"generate valid request limit": {
			getConfig: func() Config {
				os.Setenv("POD_CLEANUP_KUBERNETES_REQUEST_LIMIT", "300")
				os.Setenv("POD_CLEANUP_KUBERNETES_ANNOTATION", "pod-cleanup/ttl")
				config := New()
				os.Unsetenv("POD_CLEANUP_KUBERNETES_REQUEST_LIMIT")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_ANNOTATION")
				return config
			},
			expectedConfig: Config{
				LogLevel:             "info",
				LogFormat:            logging.FormatJSON,
				Interval:             "60s",
				CacheCleanupInterval: "30m",
				CacheExpiration:      "1.5h",
				Limit:                15,
				MaxErrAllowed:        5,
				Kubernetes: KubernetesConfig{
					RequestLimit: 300,
					Namespaces:   []string{"default"},
					Annotation:   "pod-cleanup/ttl",
				},
			},
			verify: func(t *testing.T, c, expected Config) {
				assert.Equal(t, c.GetRequestLimit(), expected.GetRequestLimit())
				assert.Equal(t, c.Kubernetes.Annotation, expected.Kubernetes.Annotation)
				assert.Equal(t, c.Kubernetes.Namespaces, expected.Kubernetes.Namespaces)
			},
		},
		"generate invalid minimum configuration": {
			getConfig: func() Config {
				os.Setenv("POD_CLEANUP_LOG_LEVEL", "debug")
				os.Setenv("POD_CLEANUP_INTERVAL", "1us")
				os.Setenv("POD_CLEANUP_CACHE_CLEANUP_INTERVAL", "10us")
				os.Setenv("POD_CLEANUP_CACHE_EXPIRATION", "10us")
				os.Setenv("POD_CLEANUP_KUBERNETES_REQUEST_TIMEOUT", "1us")
				os.Setenv("POD_CLEANUP_KUBERNETES_REQUEST_LIMIT", "5")
				os.Setenv("POD_CLEANUP_BLACKLIST_RESET", "1m")
				os.Setenv("POD_CLEANUP_KUBERNETES_ANNOTATION", "pod-cleanup/ttl")
				config := New()
				os.Unsetenv("POD_CLEANUP_LOG_LEVEL")
				os.Unsetenv("POD_CLEANUP_INTERVAL")
				os.Unsetenv("POD_CLEANUP_CACHE_CLEANUP_INTERVAL")
				os.Unsetenv("POD_CLEANUP_CACHE_EXPIRATION")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_REQUEST_TIMEOUT")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_REQUEST_LIMIT")
				os.Unsetenv("POD_CLEANUP_BLACKLIST_RESET")
				os.Unsetenv("POD_CLEANUP_KUBERNETES_ANNOTATION")
				return config
			},
			expectedConfig: Config{
				LogLevel:             "debug",
				LogFormat:            logging.FormatJSON,
				Interval:             "1us",
				CacheCleanupInterval: "10m",
				CacheExpiration:      "0.5h",
				Limit:                5,
				MaxErrAllowed:        5,
				Kubernetes: KubernetesConfig{
					RequestTimeout: "1us",
					RequestLimit:   50,
					Namespaces:     []string{"default"},
					Annotation:     "pod-cleanup/ttl",
				},
			},
			verify: func(t *testing.T, c, expected Config) {
				interval, err := c.GetInterval()
				assert.NoError(t, err)

				requestTimeout, err := c.GetRequestTimeout()
				assert.NoError(t, err)

				cacheCleanupInterval, err := c.GetCacheCleanupInterval()
				assert.NoError(t, err)

				cacheExpiration, err := c.GetCacheExpiration()
				assert.NoError(t, err)

				assert.Equal(t, interval, minimunInterval)
				assert.Equal(t, cacheCleanupInterval, minimumCacheCleanupInterval)
				assert.Equal(t, cacheExpiration, minimumCacheExpiration)
				assert.Equal(t, requestTimeout, minimumRequestTimeout)
				assert.Equal(t, c.GetRequestLimit(), minimumRequestLimit)
				assert.Equal(t, c.Kubernetes.Annotation, expected.Kubernetes.Annotation)
				assert.Equal(t, c.Kubernetes.Namespaces, expected.Kubernetes.Namespaces)
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			c := tc.getConfig()
			tc.verify(t, c, tc.expectedConfig)
		})
	}
}
